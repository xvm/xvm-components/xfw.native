﻿using System.Collections.Generic;

namespace XFWNative.Libpython.Common.Data.Signatures
{
    public class Structure
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public List<StructureSignature> Signatures { get; set; } = new List<StructureSignature>();

        public Structure()
        {
        }

    }
}
