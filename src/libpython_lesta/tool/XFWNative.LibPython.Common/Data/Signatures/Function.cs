﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace XFWNative.Libpython.Common.Data.Signatures
{
    public class Function
    {
        public string Name;
        public string Arguments;
        public string ReturnType;

        [NonSerialized]
        public bool IsInternal = false;

        [NonSerialized]
        public bool Inlined = false;

        public List<FunctionSignature> Signatures = new List<FunctionSignature>();

    }
}
