﻿using System.Collections.Generic;

namespace XFWNative.Libpython.Common.Data.Signatures
{
    public class Root
    {
        public List<Function> Functions = new List<Function>();
        public List<Structure> Structures = new List<Structure>();

        public Root()
        {
        }
    }
}
