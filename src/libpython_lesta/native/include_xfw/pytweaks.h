#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

void PyAPI_FUNC(BW_Py_DEC_REF) (void*);
void PyAPI_FUNC(BW_Py_INC_REF) (void*);

#if defined(__cplusplus)
}
#endif

#undef Py_DECREF
#define Py_DECREF(op) BW_Py_DEC_REF(op)
#undef Py_INCREF
#define Py_INCREF(op) BW_Py_INC_REF(op)


//
// DATA PTR
//

#if !defined(XFW_DISABLE_PTR)

#define _Py_EllipsisObject (*_Py_EllipsisObject)
#define _Py_NoneStruct (*_Py_NoneStruct)
#define _Py_NotImplementedStruct (*_Py_NotImplementedStruct)
#define _Py_TrueStruct (*_Py_TrueStruct)
#define _Py_ZeroStruct (*_Py_ZeroStruct)

#define PyBaseObject_Type (*PyBaseObject_Type)
#define PyBaseString_Type (*PyBaseString_Type)

#define PyBool_Type (*PyBool_Type)

#define PyBuffer_Type (*PyBuffer_Type)

#define PyByteArray_Type (*PyByteArray_Type)
#define PyByteArrayIter_Type (*PyByteArrayIter_Type)
#define _PyByteArray_empty_string (*_PyByteArray_empty_string)

#define PyCapsule_Type (*PyCapsule_Type)

#define PyCFunction_Type (*PyCFunction_Type)

#define PyClass_Type (*PyClass_Type)
#define PyClassMethod_Type (*PyClassMethod_Type)

#define PyDict_Type (*PyDict_Type)
#define PyDictIterKey_Type (*PyDictIterKey_Type)
#define PyDictIterValue_Type (*PyDictIterValue_Type)
#define PyDictIterItem_Type (*PyDictIterItem_Type)
#define PyDictKeys_Type (*PyDictKeys_Type)
#define PyDictItems_Type (*PyDictItems_Type)
#define PyDictValues_Type (*PyDictValues_Type)

#define PyEllipsis_Type (*PyEllipsis_Type)

#define PyExc_AttributeError (*PyExc_AttributeError)
#define PyExc_BufferError (*PyExc_BufferError)
#define PyExc_FutureWarning (*PyExc_FutureWarning)
#define PyExc_ImportError (*PyExc_ImportError)
#define PyExc_IndexError (*PyExc_IndexError)
#define PyExc_KeyError (*PyExc_KeyError)
#define PyExc_MemoryError (*PyExc_MemoryError)
#define PyExc_OverflowError (*PyExc_OverflowError)
#define PyExc_RuntimeError (*PyExc_RuntimeError)
#define PyExc_StopIteration (*PyExc_StopIteration)
#define PyExc_SystemError (*PyExc_SystemError)
#define PyExc_TypeError (*PyExc_TypeError)
#define PyExc_ValueError (*PyExc_ValueError)

#define PyFloat_Type (*PyFloat_Type)

#define PyFrozenSet_Type (*PyFrozenSet_Type)

#define PyFunction_Type (*PyFunction_Type)

#define PyInstance_Type (*PyInstance_Type)

#define PyMemoryView_Type (*PyMemoryView_Type)

#define PyMethod_Type (*PyMethod_Type)

#define PyModule_Type (*PyModule_Type)

#define PyProperty_Type (*PyProperty_Type)

#define PySet_Type (*PySet_Type)

#define PySlice_Type (*PySlice_Type)

#define PyStaticMethod_Type (*PyStaticMethod_Type)

#define PySuper_Type (*PySuper_Type)

#define PyString_Type (*PyString_Type)

#define _PyThreadState_Current (*_PyThreadState_Current)

#define PyType_Type (*PyType_Type)

#define PyUnicode_Type (*PyUnicode_Type)

#define _PyWeakref_RefType (*_PyWeakref_RefType)
#define _PyWeakref_ProxyType (*_PyWeakref_ProxyType)
#define _PyWeakref_CallableProxyType (*_PyWeakref_CallableProxyType)


#endif
