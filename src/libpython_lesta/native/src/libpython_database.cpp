/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2021 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdio>

#include "xfwnative_autogen_func.h"
#include "xfwnative_autogen_signature.h"
#include "xfwnative_autogen_struct.h"
#include "libpython_cache.h"
#include "libpython_database.h"
#include "libpython_memory.h"
#include "libpython_parallel.h"


void* xfwnative_database_get_function(const char* str)
{
    if (!str) {
        return NULL;
    }

    for (size_t i = 0; i < Functions_Count; i++) {
        if (strcmp(str, Functions_Names[i]) == 0) {
            return (void*)Functions_Addrs[i];
        }
    }

    return NULL;
}


void* xfwnative_database_get_structure(const char* str)
{
    if (!str) {
        return NULL;
    }

    for (size_t i = 0; i < Structures_Count; i++) {
        if (strcmp(str, Structures_Names[i]) == 0) {
            return (void*)xfwnative_autogen_struct_addrs[i];
        }
    }

    return NULL;
}


void* xfwnative_database_get(const char* str)
{
    void* result = NULL;

    if (!str) {
        return NULL;
    }


    result = xfwnative_database_get_function(str);
    if (!result) {
        result = xfwnative_database_get_structure(str);
    }

    return result;
}


bool xfwnative_database_initialize()
{
    if (!xfwnative_cache_deserialize())
    {
        if (!xfwnative_database_find_functions()) {
            return false;
        }

        if (!xfwnative_database_find_structures()) {
            return false;
        }

        xfwnative_autogen_struct_update();
        xfwnative_cache_serialize();
    }

    return true;
}

bool xfwnative_database_find_functions()
{
    bool success = true;

    //get filename
    WCHAR lpFilename[2048];
    GetModuleFileNameW(nullptr, lpFilename, sizeof(lpFilename));

    //get addr
    size_t startpos = reinterpret_cast<size_t>(GetModuleHandleW(lpFilename));
    size_t endpos = startpos + xfwnative_memory_getmodulesize(lpFilename);

    //fill functions
    ParallelFor(Functions_Count, [=, &success](int functionId) {
        if (!xfwnative_database_find_function(startpos, endpos, functionId)) {
            success = false;
        }
    });

    return success;
}

bool xfwnative_database_find_function(size_t startPosition, size_t endPosition, int functionId)
{
    size_t addr = 0;

    for (size_t sig_id = 0; sig_id < Functions_SignatureCount[functionId]; sig_id++)
    {
        addr = xfwnative_memory_findfunction(startPosition, endPosition, Functions_SignatureData[functionId][sig_id], Functions_SignatureMask[functionId][sig_id]);
        if (addr != 0) {
            Functions_Addrs[functionId] = addr;
            break;
        }
    }

    if (addr == 0) {
        char str[255]{};
        snprintf(str, 255, "XFW.Native/libpython: function not found: %s", Functions_Names[functionId]);
        OutputDebugStringA(str);
        return false;
    }

    return true;
}

bool xfwnative_database_find_structures()
{
    bool success = true;

    for (size_t struct_id = 0; struct_id < Structures_Count; struct_id++)
    {
        size_t addr = 0;

        for (size_t sig_id = 0; sig_id < Structures_SignatureCount[struct_id]; sig_id++)
        {
            auto function_addr = Functions_Addrs[Structures_SignatureFunctionIndex[struct_id][sig_id]];
            auto function_offset = Structures_SignatureOffset[struct_id][sig_id];
            addr = xfwnative_memory_read32bit(function_addr + function_offset);

            if (addr == 0) {
                continue;
            }

#ifdef _WIN64
            addr += function_addr + function_offset + 4;
#endif

            if (xfwnative_memory_validatestructure(
                Functions_Addrs[Structures_SignatureFunctionIndex[struct_id][sig_id]] + Structures_SignatureOffset[struct_id][sig_id],
                Structures_SignatureValidationPrefixData[struct_id][sig_id],
                Structures_SignatureValidationPrefixMask[struct_id][sig_id]) == FALSE) {
                addr = 0;
                continue;
            }

            xfwnative_autogen_struct_addrs[struct_id] = addr;
            break;
        }

        if (addr == 0) {
            char str[255]{};
            snprintf(str, 255, "XFW.Native/libpython: structure not found: %s", Structures_Names[struct_id]);
            OutputDebugStringA(str);
            success = false;
        }
    }

    return success;
}
