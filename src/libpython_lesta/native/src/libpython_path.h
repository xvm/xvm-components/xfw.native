#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

wchar_t* xfwnative_path_getapplicationpath();

wchar_t* xfwnative_path_getdllpath();

#ifdef __cplusplus
}
#endif
