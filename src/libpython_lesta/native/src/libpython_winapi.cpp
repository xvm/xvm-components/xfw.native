/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2021 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <Windows.h>
#include <Shlwapi.h>

#include "libpython.h"
#include "libpython_database.h"
#include "libpython_winapi.h"

//TODO #include <xfw_hooks.h>


static bool xfwnative_winapi__initialized = false;

//
// GetProcAddress
//

typedef FARPROC(WINAPI* GETPROCADDRESS)(HMODULE hModule, LPCSTR lpProcName);
GETPROCADDRESS GetProcAddress_real = NULL;


FARPROC WINAPI GetProcAddress_libpython(HMODULE hModule, LPCSTR lpProcName)
{
    FARPROC ret = NULL;
    if (hModule == PyWin_DLLhModule)
    {
        ret = (FARPROC)xfwnative_database_get(lpProcName);
        if (ret != NULL) {
            return ret;
        }
    }

    return GetProcAddress_real(hModule, lpProcName);
}


//
// LoadLibraryExA
//

typedef HMODULE(WINAPI* LOADLIBRARYEXA)(LPCSTR lpLibFileName, HANDLE hFile, DWORD dwFlags);
LOADLIBRARYEXA LoadLibraryExA_real = NULL;


HMODULE WINAPI LoadLibraryExA_libpython(LPCSTR lpLibFileName, HANDLE hFile, DWORD dwFlags)
{
    char lib_folder[MAXINT16]{};
    char lib_path[MAXINT16]{};

    //check for pyd
    if(StrStrIA(lpLibFileName, ".pyd") == NULL){ //TODO: check for the end of string
        return LoadLibraryExA_real(lpLibFileName, hFile, dwFlags);
    }

    // convert to full path    
    GetFullPathNameA(lpLibFileName, sizeof(lib_path), lib_path, NULL);

    // remove filename from fullpath
    strcpy(lib_folder, lib_path);
    PathRemoveFileSpecA(lib_folder);

    // change DLL search directory
    if (SetDllDirectoryA(lib_folder) == FALSE) {
        OutputDebugStringA("[XFW/NATIVE] LoadLibaryExA() -> failed to change DLL search directory\n");
    }

    // load library
    return LoadLibraryExA_real(lib_path, hFile, dwFlags);
}


//
// XFWNative
//

bool xfwnative_winapi__initialize() {
    WCHAR Path[MAX_PATH];
    GetModuleFileNameW(PyWin_DLLhModule, Path, MAX_PATH);
    PathRemoveFileSpecW(Path);
    wcsncat(Path, L"\\xfw_hooks.dll", 16);

    if (LoadLibraryW(Path) == NULL) {
        return false;
    }

    xfwnative_winapi__initialized = true;
    return true;
}

bool xfwnative_winapi_enable()
{

    /*
    if (!xfwnative_winapi__initialized) {
        xfwnative_winapi__initialize();
    }

    if (!XFW::Hooks::HookCreate(&GetProcAddress, &GetProcAddress_libpython, (void**)(&GetProcAddress_real))) {
        return false;
    }

    if (!XFW::Hooks::HookEnable(&GetProcAddress)) {
        return false;
    }


    if (!XFW::Hooks::HookCreate(&LoadLibraryExA, &LoadLibraryExA_libpython, (void**)(&LoadLibraryExA_real))) {
        return false;
    }

    if (!XFW::Hooks::HookEnable(&LoadLibraryExA)) {
        return false;
    }
    */

    return true;
}

bool xfwnative_winapi_disable()
{
    bool result = true;

    /*
    result = XFW::Hooks::HookDisable(&GetProcAddress) && result;
    result = XFW::Hooks::HookDisable(&LoadLibraryExA) && result;
    */

    return true;
}
