/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2021 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

void* xfwnative_database_get_function(const char* str);

void* xfwnative_database_get_structure(const char* str);

void* xfwnative_database_get(const char* str);

bool xfwnative_database_initialize();

static bool xfwnative_database_find_functions();

static bool xfwnative_database_find_function(size_t startPosition, size_t endPosition, int functionId);

static bool xfwnative_database_find_structures();

#ifdef __cplusplus
}
#endif
