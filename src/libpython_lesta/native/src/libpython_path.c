#include <Windows.h>

#include "libpython_path.h"

#include "libpython.h"

wchar_t* xfwnative_path_getapplicationpath()
{
    wchar_t* buf = malloc((MAX_PATH+1) * sizeof(wchar_t));
    GetModuleFileNameW(NULL, buf, MAX_PATH);
    return buf;
}

wchar_t* xfwnative_path_getdllpath()
{
    wchar_t* buf = malloc((MAX_PATH + 1) * sizeof(wchar_t));
    GetModuleFileNameW(PyWin_DLLhModule, buf, MAX_PATH);
    return buf;
}
