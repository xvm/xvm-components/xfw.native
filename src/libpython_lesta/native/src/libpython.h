/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2021 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef __cplusplus
extern "C" {
#endif

    #include <Windows.h>

    __declspec(dllexport) extern HMODULE PyWin_DLLhModule;

    __declspec(dllexport) extern char* PyWin_DLLVersionString;

#ifdef __cplusplus
}
#endif
