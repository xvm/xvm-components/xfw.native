/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2021 XVM contributors.
* Copyright (c) 2018-2019 Yuki Koyama.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <thread>
#include <vector>

template<typename Callable>
inline void ParallelFor(int n, Callable function, int target_concurrency = 0)
{
    std::vector<std::thread> threads;

    const int hint = (target_concurrency == 0) ? std::thread::hardware_concurrency() : target_concurrency;
    const int thread_count = std::min(n, (hint == 0) ? 4 : hint);

    const int n_max_tasks_per_thread = (n / thread_count) + (n % thread_count == 0 ? 0 : 1);
    const int n_lacking_tasks = n_max_tasks_per_thread * thread_count - n;

    auto inner_loop = [&](const int thread_index) {
        const int n_lacking_tasks_so_far = std::max(thread_index - thread_count + n_lacking_tasks, 0);
        const int inclusive_start_index = thread_index * n_max_tasks_per_thread - n_lacking_tasks_so_far;
        const int exclusive_end_index = inclusive_start_index + n_max_tasks_per_thread - (thread_index - thread_count + n_lacking_tasks >= 0 ? 1 : 0);

        for (auto k = inclusive_start_index; k < exclusive_end_index; ++k)
        {
            function(k);
        }
    };

    for (auto j = 0; j < thread_count; ++j) {
        threads.push_back(std::thread(inner_loop, j));
    }

    for (auto& t : threads) {
        t.join();
    }
}
