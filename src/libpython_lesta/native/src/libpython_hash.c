#include <stdlib.h>
#include <stdio.h>

#include <xxh3.h>

#include "libpython_hash.h"

uint64_t xfwnative_hash_calculateforfile(const wchar_t* filepath)
{
    //open file
    FILE* file_handle = _wfopen(filepath, L"rb");
    if (file_handle == NULL) {
        return 0;
    }

    //get file size
    fseek(file_handle, 0, SEEK_END);
    long file_size = ftell(file_handle);
    rewind(file_handle);
    if (file_size == 0) {
        return 0;
    }

    //read to buffer
    void* buffer = malloc(file_size);
    fread(buffer, 1, file_size, file_handle);
    fclose(file_handle);

    //calculate hash
    XXH64_hash_t hash = XXH64(buffer, file_size, 0);

    free(buffer);

    return hash;
}
