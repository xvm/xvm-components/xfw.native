#define Py_BUILD_CORE 1
#include "Python.h"

//CPython 2.7.18, exceptions.c, L#1413
int
PyUnicodeEncodeError_SetStart(PyObject *exc, Py_ssize_t start)
{
    ((PyUnicodeErrorObject *)exc)->start = start;
    return 0;
}