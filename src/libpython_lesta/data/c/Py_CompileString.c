#define Py_BUILD_CORE 1
#include "Python.h"

#undef Py_CompileString
PyAPI_FUNC(PyObject *) Py_CompileString(const char *, const char *, int);


PyObject *
Py_CompileString(const char *str, const char *p, int s)
{
    return Py_CompileStringFlags(str, p, s, NULL);
}