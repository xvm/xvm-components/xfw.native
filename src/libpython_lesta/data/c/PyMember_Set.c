#define Py_BUILD_CORE 1
#include "Python.h"
#include "structmember.h"

int
PyMember_Set(char *addr, struct memberlist *mlist, const char *name, PyObject *v)
{
    struct memberlist *l;

    for (l = mlist; l->name != NULL; l++) {
        if (strcmp(l->name, name) == 0) {
            PyMemberDef copy;
            copy.name = l->name;
            copy.type = l->type;
            copy.offset = l->offset;
            copy.flags = l->flags;
            copy.doc = NULL;
            return PyMember_SetOne(addr, &copy, v);
        }
    }

    PyErr_SetString(PyExc_AttributeError, name);
    return -1;
}