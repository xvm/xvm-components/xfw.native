//PyUnicode_FromFormat
//Python 2.7.16

#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(PyObject *) PyUnicode_FromFormat(const char *format, ...);

PyObject *
PyUnicode_FromFormat(const char *format, ...)
{
    PyObject* ret;
    va_list vargs;

#ifdef HAVE_STDARG_PROTOTYPES
    va_start(vargs, format);
#else
    va_start(vargs);
#endif
    ret = PyUnicode_FromFormatV(format, vargs);
    va_end(vargs);
    return ret;
}
