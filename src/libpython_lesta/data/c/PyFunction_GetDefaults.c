#define Py_BUILD_CORE 1
#include "Python.h"

#undef PyFunction_GetDefaults
PyAPI_FUNC(PyObject *) PyFunction_GetDefaults(PyObject *);

PyObject *
PyFunction_GetDefaults(PyObject *op)
{
    if (!PyFunction_Check(op)) {
        PyErr_BadInternalCall();
        return NULL;
    }
    return ((PyFunctionObject *) op) -> func_defaults;
}