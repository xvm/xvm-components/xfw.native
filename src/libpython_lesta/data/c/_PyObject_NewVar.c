#define Py_BUILD_CORE 1
#include "Python.h"

//CPython 2.7.18, object.c, L#250
PyVarObject*
_PyObject_NewVar(PyTypeObject* tp, Py_ssize_t nitems)
{
    PyVarObject* op;
    const size_t size = _PyObject_VAR_SIZE(tp, nitems);
    op = (PyVarObject*)PyObject_MALLOC(size);
    if (op == NULL)
        return (PyVarObject*)PyErr_NoMemory();
    return PyObject_INIT_VAR(op, tp, nitems);
}