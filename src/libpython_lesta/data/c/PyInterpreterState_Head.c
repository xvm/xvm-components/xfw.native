#define Py_BUILD_CORE 1

#include "Python.h"
#include "pythread.h"

extern PyInterpreterState** interp_head;

PyInterpreterState *
PyInterpreterState_Head(void)
{
    return *interp_head;
}