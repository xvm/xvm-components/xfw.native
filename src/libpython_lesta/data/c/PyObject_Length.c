#define Py_BUILD_CORE 1
#include "Python.h"


//CPython 2.7.18, abstract.c, L#77
#undef PyObject_Length
Py_ssize_t
PyObject_Length(PyObject *o)
{
    return PyObject_Size(o);
}
