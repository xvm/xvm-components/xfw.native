#define Py_BUILD_CORE 1

#include "Python.h"

int _PyUnicodeUCS2_IsLinebreak(register const Py_UNICODE ch)
{
    switch (ch) {
    case 0x000A:
    case 0x000B:
    case 0x000C:
    case 0x000D:
    case 0x001C:
    case 0x001D:
    case 0x001E:
    case 0x0085:
    case 0x2028:
    case 0x2029:
        return 1;
    }
    return 0;
}