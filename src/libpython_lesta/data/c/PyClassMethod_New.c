#define Py_BUILD_CORE 1
#include "Python.h"

typedef struct {
    PyObject_HEAD
    PyObject *cm_callable;
} classmethod;

PyObject *
PyClassMethod_New(PyObject *callable)
{
    classmethod *cm = (classmethod *)
        PyType_GenericAlloc(&PyClassMethod_Type, 0);
    if (cm != NULL) {
        Py_INCREF(callable);
        cm->cm_callable = callable;
    }
    return (PyObject *)cm;
}