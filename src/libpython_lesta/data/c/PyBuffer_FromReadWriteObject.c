#define Py_BUILD_CORE 1
#include "Python.h"

//CPython 2.7.18, bufferobject.c, L#7
typedef struct {
    PyObject_HEAD
        PyObject* b_base;
    void* b_ptr;
    Py_ssize_t b_size;
    Py_ssize_t b_offset;
    int b_readonly;
    long b_hash;
} PyBufferObject;


//CPython 2.7.18, bufferobject.c, L#98
static PyObject*
buffer_from_memory(PyObject* base, Py_ssize_t size, Py_ssize_t offset, void* ptr,
    int readonly)
{
    PyBufferObject* b;

    if (size < 0 && size != Py_END_OF_BUFFER) {
        PyErr_SetString(PyExc_ValueError,
            "size must be zero or positive");
        return NULL;
    }
    if (offset < 0) {
        PyErr_SetString(PyExc_ValueError,
            "offset must be zero or positive");
        return NULL;
    }

    b = PyObject_NEW(PyBufferObject, &PyBuffer_Type);
    if (b == NULL)
        return NULL;

    Py_XINCREF(base);
    b->b_base = base;
    b->b_ptr = ptr;
    b->b_size = size;
    b->b_offset = offset;
    b->b_readonly = readonly;
    b->b_hash = -1;

    return (PyObject*)b;
}

//CPython 2.7.18, bufferobject.c, L#130
static PyObject*
buffer_from_object(PyObject* base, Py_ssize_t size, Py_ssize_t offset, int readonly)
{
    if (offset < 0) {
        PyErr_SetString(PyExc_ValueError,
            "offset must be zero or positive");
        return NULL;
    }
    if (PyBuffer_Check(base) && (((PyBufferObject*)base)->b_base)) {
        /* another buffer, refer to the base object */
        PyBufferObject* b = (PyBufferObject*)base;
        if (b->b_size != Py_END_OF_BUFFER) {
            Py_ssize_t base_size = b->b_size - offset;
            if (base_size < 0)
                base_size = 0;
            if (size == Py_END_OF_BUFFER || size > base_size)
                size = base_size;
        }
        offset += b->b_offset;
        base = b->b_base;
    }
    return buffer_from_memory(base, size, offset, NULL, readonly);
}

//CPython 2.7.18, bufferobject.c, L#171
PyObject*
PyBuffer_FromReadWriteObject(PyObject* base, Py_ssize_t offset, Py_ssize_t size)
{
    PyBufferProcs* pb = base->ob_type->tp_as_buffer;

    if (pb == NULL ||
        pb->bf_getwritebuffer == NULL ||
        pb->bf_getsegcount == NULL)
    {
        PyErr_SetString(PyExc_TypeError, "buffer object expected");
        return NULL;
    }

    return buffer_from_object(base, size, offset, 0);
}