#define Py_BUILD_CORE 1

#include "Python.h"
#include "pythread.h"

extern PyThread_type_lock* interpreter_lock;

int
PyEval_ThreadsInitialized(void)
{
    return (*interpreter_lock) != 0;
}