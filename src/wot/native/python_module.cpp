// SPDX-License-Identifier: MIT
// Copyright (c) 2022 XVM Contributors

//
// Imports
//

// stdlib
#include <cstdlib>
#include <string>

// pybind11
#include <pybind11/pybind11.h>



//
// Globals
//

std::wstring g_path_default;



//
// Functions
//

std::wstring path_get(){
    return _wgetenv(L"PATH");
}

bool path_set(const std::wstring& path){
    return _wputenv(path.c_str()) == 0;
}

bool path_prepend(const std::wstring& path){
    auto path_new = L"PATH=" + path + L";" + path_get();
    return path_set(path_new);
}

bool path_reset(){
    return path_set(g_path_default);
}

void init() {
    g_path_default = path_get();

}



//
// Module
//

PYBIND11_MODULE(XFW_Native, m) {
    m.doc() = "XFW Native module";
    m.def("init", &init);
    m.def("path_get", &path_get);
    m.def("path_prepend", &path_prepend);
    m.def("path_reset", &path_reset);
}
